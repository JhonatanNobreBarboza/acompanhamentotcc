/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.acompanhamentotcc.controller;

import br.com.acompanhamentotcc.dao.AlunoDao;
import br.com.acompanhamentotcc.model.Aluno;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import org.jdesktop.observablecollections.ObservableCollections;

/**
 *
 * @author Jhonatan Nobre
 */
public class AlunoController {
    private PropertyChangeSupport propertyChangeSupport 
            = new PropertyChangeSupport(this);
    private Aluno alunoDigitado;
    private Aluno alunoSelecionado;
    private List<Aluno> alunosTabela;
    private AlunoDao alunoDao;

    public AlunoController() {
        alunoDao = new AlunoDao();
        alunosTabela = ObservableCollections.
                observableList(new ArrayList<Aluno>());
        novo();
        pesquisar();
    }
    
    public void novo(){
        setAlunoDigitado(new Aluno());
    }
    
    public void pesquisar(){
        alunosTabela.clear();
        alunosTabela.addAll(alunoDao.pesquisar(alunoDigitado));
    }
    
    public void salvar() {
        alunoDao.create(alunoDigitado);
        novo();
        pesquisar();
    }
    public void excluir(){
        alunoDao.excluir(alunoDigitado);
        novo();
        pesquisar();
    }
    
    public Aluno getAlunoDigitado(){
        return alunoDigitado;
    }
    
    public void setAlunoDigitado(Aluno alunoDigitado){
        Aluno oldAlunoDigitado = this.alunoDigitado;
        this.alunoDigitado = alunoDigitado;
        propertyChangeSupport.firePropertyChange("alunoDigitado", 
                oldAlunoDigitado,alunoDigitado);
    }
    
    public Aluno getAlunoSelecionado(){
        return alunoSelecionado;
    }
    
    public void setAlunoSelecionado(Aluno alunoSelecionado){
        this.alunoSelecionado = alunoSelecionado;
        if(this.alunoSelecionado != null){
            setAlunoDigitado(alunoSelecionado);
        }
    }
    
    public List<Aluno> getAlunosTabela(){
        return alunosTabela;
    }
    
    public void setAlunosTabela(List<Aluno> cursosTabela){
        this.alunosTabela = alunosTabela;
    }
    
    public void addPropertyChangeListener(PropertyChangeListener pl){
        propertyChangeSupport.addPropertyChangeListener(pl);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener pl){        
        propertyChangeSupport.removePropertyChangeListener(pl);
    }
}
