/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.acompanhamentotcc.controller;

import br.com.acompanhamentotcc.dao.DisciplinaDao;
import br.com.acompanhamentotcc.model.Disciplina;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import org.jdesktop.observablecollections.ObservableCollections;

/**
 *
 * @author Jhonatan Nobre
 */
public class DisciplinaController {
    private PropertyChangeSupport propertyChangeSupport 
            = new PropertyChangeSupport(this);
    private Disciplina disciplinaDigitado;
    private Disciplina disciplinaSelecionado;
    private List<Disciplina> disciplinasTabela;
    private DisciplinaDao disciplinaDao;

    public DisciplinaController() {
        disciplinaDao = new DisciplinaDao();
        disciplinasTabela = ObservableCollections.
                observableList(new ArrayList<Disciplina>());
        novo();
        pesquisar();
    }
    
    public void novo(){
        setDisciplinaDigitado(new Disciplina());
    }
    
    public void pesquisar(){
        disciplinasTabela.clear();
        disciplinasTabela.addAll(disciplinaDao.pesquisar(disciplinaDigitado));
    }
    
    public void salvar() {
        disciplinaDao.create(disciplinaDigitado);
        novo();
        pesquisar();
    }
    public void excluir(){
        disciplinaDao.excluir(disciplinaDigitado);
        novo();
        pesquisar();
    }
    
    public Disciplina getDisciplinaDigitado(){
        return disciplinaDigitado;
    }
    
    public void setDisciplinaDigitado(Disciplina disciplinaDigitado){
        Disciplina oldDisciplinaDigitado = this.disciplinaDigitado;
        this.disciplinaDigitado = disciplinaDigitado;
        propertyChangeSupport.firePropertyChange("disciplinaDigitado", 
                oldDisciplinaDigitado,disciplinaDigitado);
    }
    
    public Disciplina getDisciplinaSelecionado(){
        return disciplinaSelecionado;
    }
    
    public void setDisciplinaSelecionado(Disciplina disciplinaSelecionado){
        this.disciplinaSelecionado = disciplinaSelecionado;
        if(this.disciplinaSelecionado != null){
            setDisciplinaDigitado(disciplinaSelecionado);
        }
    }
    
    public List<Disciplina> getDisciplinasTabela(){
        return disciplinasTabela;
    }
    
    public void setDisciplinasTabela(List<Disciplina> disciplinasTabela){
        this.disciplinasTabela = disciplinasTabela;
    }
    
    public void addPropertyChangeListener(PropertyChangeListener pl){
        propertyChangeSupport.addPropertyChangeListener(pl);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener pl){        
        propertyChangeSupport.removePropertyChangeListener(pl);
    }
}
