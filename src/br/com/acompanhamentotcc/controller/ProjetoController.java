/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.acompanhamentotcc.controller;

import br.com.acompanhamentotcc.dao.AlunoDao;
import br.com.acompanhamentotcc.dao.EducadorDao;
import br.com.acompanhamentotcc.dao.ProjetoDao;
import br.com.acompanhamentotcc.model.Aluno;
import br.com.acompanhamentotcc.model.Educador;
import br.com.acompanhamentotcc.model.Projeto;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import org.jdesktop.observablecollections.ObservableCollections;

/**
 *
 * @author Jhonatan Nobre
 */
public final class ProjetoController {
    private PropertyChangeSupport propertyChangeSupport 
            = new PropertyChangeSupport(this);
    
    private Projeto projetoDigitado;
    private Projeto projetoSelecionado;
    private ProjetoDao projetoDao;
    private ProjetoDao projDao;
    
    private Educador orientadorDigitado;
    private Educador orientadorSelecionado;
    private EducadorDao orientadorDao;
    private Educador orientador;
    
    private Educador coorientadorDigitado;
    private Educador coorientadorSelecionado;
    private EducadorDao coorientadorDao;
    private Educador coorientador;
    
    
    private AlunoDao alunoDao; 
    private Aluno alunoDigitado;
    private Aluno alunoSelecionado;
    private Aluno aluno;
    
    private List<Aluno> alunos;
    private List<Aluno> alunosCombo;
    private List<Aluno> alunosTabela;
    
    private List<Educador> orientadores;
    
    private List<Educador> coorientadores;
    private List<Educador> coorientadoresCombo;
    private List<Educador> coorientadoresTabela;
    
    private List<Projeto> projetosTabela;
    private List<Projeto> projetos;
    
    public ProjetoController() {
        alunos = ObservableCollections.
                observableList(new ArrayList<>());
        coorientadores = ObservableCollections.
                observableList(new ArrayList<>());        
        orientadores = ObservableCollections.
                observableList(new ArrayList<>());
        projetosTabela = ObservableCollections.
                observableList(new ArrayList<>());
        coorientadoresTabela = ObservableCollections.
                observableList(new ArrayList<>());
        
        projetoDao = new ProjetoDao();
        orientadorDao = new EducadorDao();
        coorientadorDao = new EducadorDao();
        alunoDao = new AlunoDao();
        orientador = new Educador();
        coorientador = new Educador();
        projDao = new ProjetoDao();
        alunosCombo = new AlunoDao().findAlunoEntities();
        coorientadoresCombo = new EducadorDao().findEducadorEntities();
        novo();
        pesquisar();
    }
    
    public void novo(){
        alunos.clear();
        coorientadores.clear();
        setProjetoDigitado(new Projeto());
    }
    
    public void pesquisar(){
        projetosTabela.clear();
        projetosTabela.addAll(projetoDao.pesquisar(projetoDigitado));
    }
    
    public void addAluno(){
        alunos.add(alunoSelecionado);
        alunosCombo.remove(alunoSelecionado);
    }
    
    public void addCoorientador(){
        coorientadores.add(coorientadorSelecionado);
        coorientadoresCombo.remove(coorientadorSelecionado);
    }
    
    public void salvar() {
        projetoDigitado.setAlunos(alunos);
        projetoDigitado.setOrientadores(orientadorSelecionado);
        projetoDigitado.setCoorientadores(coorientadores);
        projDao.create(projetoDigitado);
        novo();        
        projetoDigitado.setOrientadores(orientadorSelecionado);
        projetoDigitado.setCoorientadores(coorientadores);
        pesquisar();
    }
    public void excluir(){
        projetoDao.excluir(projetoSelecionado);
        novo();
        pesquisar();
    }

    public List<Aluno> getAlunos() {
        return alunos;
    }

    public void setAlunos(List<Aluno> alunos) {
        this.alunos = alunos;
    }

    public List<Projeto> getProjetos() {
        return projetos;
    }

    public void setProjetos(List<Projeto> projetos) {
        this.projetos = projetos;
    }

    public ProjetoDao getProjDao() {
        return projDao;
    }

    public void setProjDao(ProjetoDao projDao) {
        this.projDao = projDao;
    }

    public List<Aluno> getAlunosCombo() {
        return alunosCombo;
    }

    public void setAlunosCombo(List<Aluno> alunosCombo) {
        this.alunosCombo = alunosCombo;
    }

    public List<Educador> getCoorientadoresTabela() {
        return coorientadoresTabela;
    }

    public void setCoorientadoresTabela(List<Educador> coorientadoresTabela) {
        this.coorientadoresTabela = coorientadoresTabela;
    }

    public Educador getCoorientadorDigitado() {
        return coorientadorDigitado;
    }

    public void setCoorientadorDigitado(Educador coorientadorDigitado) {
        this.coorientadorDigitado = coorientadorDigitado;
    }
    
    public Aluno getAlunoDigitado() {
        return alunoDigitado;
    }

    public void setAlunoDigitado(Aluno alunoDigitado) {
        this.alunoDigitado = alunoDigitado;
    }

    public Aluno getAlunoSelecionado() {
        return alunoSelecionado;
    }

    public void setAlunoSelecionado(Aluno alunoSelecionado) {
        this.alunoSelecionado = alunoSelecionado;
    }
    
    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Educador getOrientador() {
        return orientador;
    }

    public void setOrientador(Educador orientador) {
        this.orientador = orientador;
    }
    
    public List<Aluno> getAlunosTabela() {
        return alunosTabela;
    }

    public void setAlunosTabela(List<Aluno> alunosTabela) {
        this.alunosTabela = alunosTabela;
    }

    public AlunoDao getAlunoDao() {
        return alunoDao;
    }

    public void setAlunoDao(AlunoDao alunoDao) {
        this.alunoDao = alunoDao;
    }

    public ProjetoDao getProjetoDao() {
        return projetoDao;
    }

    public void setProjetoDao(ProjetoDao projetoDao) {
        this.projetoDao = projetoDao;
    }  
    
    public Projeto getProjetoDigitado(){
        return projetoDigitado;
    }
    
    public void setProjetoDigitado(Projeto projetoDigitado){
        Projeto oldProjetoDigitado = this.projetoDigitado;
        this.projetoDigitado = projetoDigitado;
        propertyChangeSupport.firePropertyChange("projetoDigitado", 
                oldProjetoDigitado,projetoDigitado);
    }

    public Projeto getProjetoSelecionado(){
        return projetoSelecionado;
    }

    public Educador getOrientadorDigitado() {
        return orientadorDigitado;
    }

    public void setOrientadorDigitado(Educador orientadorDigitado) {
        this.orientadorDigitado = orientadorDigitado;
    }

    public Educador getOrientadorSelecionado() {
        return orientadorSelecionado;
    }

    public void setOrientadorSelecionado(Educador orientadorSelecionado) {
        this.orientadorSelecionado = orientadorSelecionado;
    }

    public List<Educador> getOrientadores() {
        return orientadores;
    }

    public void setOrientadores(List<Educador> orientadores) {
        this.orientadores = orientadores;
    }

    public EducadorDao getOrientadorDao() {
        return orientadorDao;
    }

    public void setOrientadorDao(EducadorDao orientadorDao) {
        this.orientadorDao = orientadorDao;
    }

    public EducadorDao getCoorientadorDao() {
        return coorientadorDao;
    }

    public void setCoorientadorDao(EducadorDao coorientadorDao) {
        this.coorientadorDao = coorientadorDao;
    }

    public void setProjetoSelecionado(Projeto projetoSelecionado){
        this.projetoSelecionado = projetoSelecionado;
        if(this.projetoSelecionado != null){
            setProjetoDigitado(projetoSelecionado);
        }
    }
    
    public List<Projeto> getProjetosTabela(){
        return projetosTabela;
    }
    
    public void setProjetosTabela(List<Projeto> projetosTabela){
        this.projetosTabela = projetosTabela;
    }
    
    public Educador getCoorientadorSelecionado() {
        return coorientadorSelecionado;
    }

    public void setCoorientadorSelecionado(Educador coorientadorSelecionado) {
        this.coorientadorSelecionado = coorientadorSelecionado;
    }

    public List<Educador> getCoorientadores() {
        return coorientadores;
    }

    public void setCoorientadores(List<Educador> coorientadores) {
        this.coorientadores = coorientadores;
    }

    public List<Educador> getCoorientadoresCombo() {
        return coorientadoresCombo;
    }

    public void setCoorientadoresCombo(List<Educador> coorientadoresCombo) {
        this.coorientadoresCombo = coorientadoresCombo;
    }

    public Educador getCoorientador() {
        return coorientador;
    }

    public void setCoorientador(Educador coorientador) {
        this.coorientador = coorientador;
    }
    
    public void addPropertyChangeListener(PropertyChangeListener pl){
        propertyChangeSupport.addPropertyChangeListener(pl);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener pl){        
        propertyChangeSupport.removePropertyChangeListener(pl);
    }

    public PropertyChangeSupport getPropertyChangeSupport() {
        return propertyChangeSupport;
    }

    public void setPropertyChangeSupport(PropertyChangeSupport propertyChangeSupport) {
        this.propertyChangeSupport = propertyChangeSupport;
    }

}
