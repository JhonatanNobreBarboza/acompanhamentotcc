/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.acompanhamentotcc.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author Jhonatan Nobre
 */
@Entity
public class Projeto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
     
    private String nomeProjeto;

    private String descProj;
    
    private Double nota;
    
    private String cargaH;
     
    @OneToMany
    private List<Aluno> alunos;
    
    @OneToOne
    private Educador orientadores;
    
    
    @ManyToMany
    @JoinTable(name = "coorientadores_do_projeto",
            joinColumns = {@JoinColumn(name = "id_coorientador")},
            inverseJoinColumns = { @JoinColumn(name = "id_projeto")})
    private List<Educador> coorientadores;

    public Educador getOrientadores() {
        return orientadores;
    }

    public void setOrientadores(Educador orientadores) {
        this.orientadores = orientadores;
    }

    public List<Educador> getEducadores() {
        return coorientadores;
    }

    public List<Educador> getCoorientadores() {
        return coorientadores;
    }

    public void setCoorientadores(List<Educador> coorientadores) {
        this.coorientadores = coorientadores;
    }
    
    public List<Aluno> getAlunos() {
        return alunos;
    }

    public Double getNota() {
        return nota;
    }

    public void setNota(Double nota) {
        this.nota = nota;
    }

    public String getCargaH() {
        return cargaH;
    }

    public void setCargaH(String cargaH) {
        this.cargaH = cargaH;
    }

    public void setAlunos(List<Aluno> alunos) {
        this.alunos = alunos;
    }
    
    public String getDescProj() {
        return descProj;
    }

    public void setDescProj(String DescProj) {
        this.descProj = DescProj;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    
    public void setNome(String nome) {
        this.nomeProjeto = nome;
    }

    public String getNome() {
        return nomeProjeto;
    }    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Projeto)) {
            return false;
        }
        Projeto other = (Projeto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.nomeProjeto;
    }
}
