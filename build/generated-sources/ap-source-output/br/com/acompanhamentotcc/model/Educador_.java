package br.com.acompanhamentotcc.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Educador.class)
public abstract class Educador_ {

	public static volatile SingularAttribute<Educador, String> siap;
	public static volatile SingularAttribute<Educador, String> areaAtuacao;
	public static volatile SingularAttribute<Educador, String> nome;
	public static volatile SingularAttribute<Educador, Long> id;

}

